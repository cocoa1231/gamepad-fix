# gamepad-fix

A Python script I found that fixes eurogames gamepad on Linux. This is **not my work**.

You'll need `pyusb` for this to work, along with `xboxdrv` install using your package manager

```
    pip3 install pyusb -U
```

Plug in your controller and run
```
    sudo python3 fixcontroller.py
```
